<html>
<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css">
    <link href="https://fonts.googleapis.com/css?family=Lato" rel="stylesheet" type="text/css">
    <link href="https://fonts.googleapis.com/css?family=Montserrat" rel="stylesheet" type="text/css">
    <link href="https://maxcdn.bootstrapcdn.com/font-awesome/4.3.0/css/font-awesome.min.css" rel="stylesheet">
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.2.1/jquery.min.js"></script>
    <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js"></script>
    <title>Update Rankings</title>
    <style>
        .error {
            color: #FF0000;
        }
        .carousel-control.right, .carousel-control.left {
            background-image: none;
            color: #f4511e;
        }
        .carousel-indicators li {
            border-color: #f4511e;
        }
        .carousel-indicators li.active {
            background-color: #f4511e;
        }
        .bg-grey {
            background-color: #f6f6f6;
        }
        .marq {
            color: aqua;background-color: black;
        }
        .navbar {
            margin-bottom: 50px;
            border-radius: 0;
        }
        .btn-success:hover {
            border: 1px solid limegreen;
            background-color: #fff;
            color: limegreen;
            transition: .2s;
        }
        .btn-danger:hover {
            border: 1px solid red;
            background-color: #fff;
            color: red;
            transition: .2s;
        }
        .btn-warning:hover {
            border: 1px solid darkorange;
            background-color: #fff;
            color: darkorange;
            transition: .2s;
        }
        .btn-primary:hover {
            border: 1px solid deepskyblue;
            background-color: #fff;
            color: deepskyblue;
            transition: .2s;
        }
        .btn-info:hover {
            border: 1px solid cyan;
            background-color: #fff;
            color: cyan;
            transition: .2s;
        }
        .btn-default:hover {
            background-color: #333;
            color: #f1f1f1;
            border: 1px solid lightslategray;
            transition: .2s;
        }
        body {

            color: #777;
        }
        h3, h4 {
            text-shadow: 0 0 6px #FF0000, 0 0 10px #0000FF;
            letter-spacing: 1px;
            font-size: 20px;

        }


        .carousel-inner img {
            -webkit-filter: grayscale(90%);
            filter: grayscale(90%); /* make all photos black and white */
            width: 100%; /* Set width to 100% */
            margin: auto;
        }
        .carousel-caption h3 {
            color: cyan !important;
        }

        .bg-1 {
            background: #2d2d30;
            color: #bdbdbd;
        }
        .bg-1 h3 {color: #fff;}
        .bg-1 p {font-style: italic;}
        .list-group-item:first-child {
            border-top-right-radius: 0;
            border-top-left-radius: 0;
        }
        .list-group-item:last-child {
            border-bottom-right-radius: 0;
            border-bottom-left-radius: 0;
        }
        .thumbnail {
            padding: 0 0 15px 0;
            border: none;
            border-radius: 0;
        }
        .thumbnail p {
            margin-top: 15px;
            color: #555;
        }

        .modal-header,  .close {
            background-color: #333;
            color: #fff !important;
            text-align: center;
            font-size: 30px;
        }
        .modal-header, .modal-body {
            padding: 40px 50px;
        }
        .nav-tabs li a {
            color: #777;
        }

        .navbar {
            font-family: Montserrat, sans-serif;
            margin-bottom: 0;
            background-color: #2d2d30;
            border: 0;
            font-size: 11px !important;
            letter-spacing: 4px;
            opacity: 0.9;
        }
        .navbar li a, .navbar .navbar-brand {
            color: #d5d5d5 !important;
        }
        .navbar-nav li a:hover {
            color: #fff !important;
        }
        .navbar-nav li.active a {
            color: cyan !important;
            background-color: darkslategrey !important;
        }
        .navbar-default .navbar-toggle {
            border-color: transparent;
        }
        .open .dropdown-toggle {
            color: #fff;
            background-color: #555 !important;
        }
        .dropdown-menu li a {
            color: #000 !important;
        }
        .dropdown-menu li a:hover {
            background-color: red !important;
        }
        footer {
            background-color: #2d2d30;
            color: #f5f5f5;
            padding: 32px;
        }
        footer a {
            color: #f5f5f5;
        }
        footer a:hover {
            color: #777;
            text-decoration: none;
        }


    </style>
</head>
<body id="myPage" data-spy="scroll" data-target=".navbar" data-offset="50" >

<nav class="navbar navbar-default navbar-fixed-top">
    <div class="container-fluid">
        <div class="navbar-header">
            <button type="button" class="navbar-toggle" data-toggle="collapse" data-target="#myNavbar">
                <span class="icon-bar"></span>
                <span class="icon-bar"></span>
                <span class="icon-bar"></span>
            </button>
            <a class="navbar-brand" href="#myPage">Umang Gupta</a>
        </div>
        <div class="collapse navbar-collapse" id="myNavbar">
            <ul class="nav navbar-nav navbar-right">
                <li><a href="http://localhost:63342/UniversityRankingSystem/page.html"> <span class="glyphicon glyphicon-chevron-left"></span> BACK TO HOME</a></li>
                <li><a href="#see">UPDATION </a></li>
                <!--                <li><a href="#info"><span class="fa fa-info-circle"></span></a></li>-->
            </ul>
        </div>
    </div>
</nav>
<div id="myCarousel" class="carousel slide" data-ride="carousel">

    <!-- Wrapper for slides -->
    <div class="carousel-inner" role="listbox">
        <div class="item active">
            <img src="universities-spain.jpg" alt="Universities" width="1200" height="100">

            <div class="carousel-caption" style="margin-bottom: 250px">
                <h3>University Ranking System Database</h3>
                <p style="color: gold;text-shadow: 0 0 3px lime">The world's top Universities compared on various metrics.</p>

                <div class="panel panel-primary">
                    <div id="see"class="panel-body" style="background-color: #2d2d30">



<?php
$servername = "127.0.0.1";
$username = "root";
$password = '';
$db= "myDB";
// Create connection
$conn = new mysqli($servername, $username, $password, $db);
// Check connection
if ($conn->connect_error) {
    die("Connection failed: " . $conn->connect_error);
} 


$sql = "Update University set {$_POST['field_to_update']}='{$_POST['new_value']}' where {$_POST['field_to_search']}='{$_POST['value_to_search']}'";

if ($conn->query($sql) === TRUE) {
    echo "<p>Table Rankings Updated successfully</p>";
} else {
    echo "Error updating table: " .$sql."<br>". $conn->error;
}
echo "<a class='btn btn-default' href='http://localhost:63342/UniversityRankingSystem/select2.php'>Click to See Updated Rankings!</a><br>";

$conn->close();
?>
                    </div>
                </div>



                <!-- Footer -->
                <footer class="text-center" style="margin-left: -345px;width: 1500px;margin-bottom: -300px">
                    <div class="container bootstrap snippet">

                        <div class="alert alert-success alert-white rounded">
                            <button type="button" data-dismiss="alert" aria-hidden="true" class="close">×</button>
                            <span style="text-shadow: none">
            <i class="fa fa-check"></i>

        <strong>Success!</strong>
            Changes have been updated successfully!
        </span>
                        </div>

                    </div>
                    <a class="up-arrow" href="#myPage" data-toggle="tooltip" title="TO TOP">
                        <span class="glyphicon glyphicon-chevron-up"></span>
                    </a><br><br>
                    <p style="text-shadow: none">University Ranking System, Made By <a data-toggle="tooltip" title="Umang Gupta 106114098">Umang Gupta (106114098)</a></p>
                </footer>
            </div>  </div>  </div> </div>


</body>
</html>
