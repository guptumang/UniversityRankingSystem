<html>
<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css">
    <link href="https://fonts.googleapis.com/css?family=Lato" rel="stylesheet" type="text/css">
    <link href="https://fonts.googleapis.com/css?family=Montserrat" rel="stylesheet" type="text/css">
    <link href="https://maxcdn.bootstrapcdn.com/font-awesome/4.3.0/css/font-awesome.min.css" rel="stylesheet">
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.2.1/jquery.min.js"></script>
    <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js"></script>
    <title>Search Results</title>
    <style>
        .error {
            color: #FF0000;
        }
        .carousel-control.right, .carousel-control.left {
            background-image: none;
            color: #f4511e;
        }
        .carousel-indicators li {
            border-color: #f4511e;
        }
        .carousel-indicators li.active {
            background-color: #f4511e;
        }
        .bg-grey {
            background-color: #f6f6f6;
        }
        .marq {
            color: aqua;background-color: black;
        }
        .navbar {
            margin-bottom: 50px;
            border-radius: 0;
        }
        .btn-success:hover {
            border: 1px solid limegreen;
            background-color: #fff;
            color: limegreen;
            transition: .2s;
        }
        .btn-danger:hover {
            border: 1px solid red;
            background-color: #fff;
            color: red;
            transition: .2s;
        }
        .btn-warning:hover {
            border: 1px solid darkorange;
            background-color: #fff;
            color: darkorange;
            transition: .2s;
        }
        .btn-primary:hover {
            border: 1px solid deepskyblue;
            background-color: #fff;
            color: deepskyblue;
            transition: .2s;
        }
        .btn-info:hover {
            border: 1px solid cyan;
            background-color: #fff;
            color: cyan;
            transition: .2s;
        }
        .btn-default:hover {
            background-color: #333;
            color: #f1f1f1;
            border: 1px solid lightslategray;
            transition: .2s;
        }
        body {

            color: #777;
        }
        h3, h4 {
            text-shadow: 0 0 6px #FF0000, 0 0 10px #0000FF;
            letter-spacing: 1px;
            font-size: 20px;

        }


        .carousel-inner img {
            -webkit-filter: grayscale(90%);
            filter: grayscale(90%); /* make all photos black and white */
            width: 100%; /* Set width to 100% */
            margin: auto;
        }
        .carousel-caption h3 {
            color: cyan !important;
        }
        @media (max-width: 600px) {
            .carousel-caption {
                display: none; /* Hide the carousel text when the screen is less than 600 pixels wide */
            }
        }
        .bg-1 {
            background: #2d2d30;
            color: #bdbdbd;
        }
        .bg-1 h3 {color: #fff;}
        .bg-1 p {font-style: italic;}
        .list-group-item:first-child {
            border-top-right-radius: 0;
            border-top-left-radius: 0;
        }
        .list-group-item:last-child {
            border-bottom-right-radius: 0;
            border-bottom-left-radius: 0;
        }
        .thumbnail {
            padding: 0 0 15px 0;
            border: none;
            border-radius: 0;
        }
        .thumbnail p {
            margin-top: 15px;
            color: #555;
        }

        .modal-header,  .close {
            background-color: #333;
            color: #fff !important;
            text-align: center;
            font-size: 30px;
        }
        .modal-header, .modal-body {
            padding: 40px 50px;
        }
        .nav-tabs li a {
            color: #777;
        }

        .navbar {
            font-family: Montserrat, sans-serif;
            margin-bottom: 0;
            background-color: #2d2d30;
            border: 0;
            font-size: 11px !important;
            letter-spacing: 4px;
            opacity: 0.9;
        }
        .navbar li a, .navbar .navbar-brand {
            color: #d5d5d5 !important;
        }
        .navbar-nav li a:hover {
            color: #fff !important;
        }
        .navbar-nav li.active a {
            color: cyan !important;
            background-color: darkslategrey !important;
        }
        .navbar-default .navbar-toggle {
            border-color: transparent;
        }
        .open .dropdown-toggle {
            color: #fff;
            background-color: #555 !important;
        }
        .dropdown-menu li a {
            color: #000 !important;
        }
        .dropdown-menu li a:hover {
            background-color: red !important;
        }
        footer {
            background-color: #2d2d30;
            color: #f5f5f5;
            padding: 32px;
        }
        footer a {
            color: #f5f5f5;
        }
        footer a:hover {
            color: #777;
            text-decoration: none;
        }
        .parallax {
            /* The image used */
            background-image: url("universities-spain.jpg");

            /* Set a specific height */
            min-height: 500px;

            /* Create the parallax scrolling effect */
            background-attachment: fixed;
            background-position: center;
            background-repeat: no-repeat;
            background-size: cover;
        }


    </style>
</head>
<body id="myPage" data-spy="scroll" data-target=".navbar" data-offset="50" class="parallax">

<nav class="navbar navbar-default navbar-fixed-top">
    <div class="container-fluid">
        <div class="navbar-header">
            <button type="button" class="navbar-toggle" data-toggle="collapse" data-target="#myNavbar">
                <span class="icon-bar"></span>
                <span class="icon-bar"></span>
                <span class="icon-bar"></span>
            </button>
            <a class="navbar-brand" href="#myPage">Umang Gupta</a>
        </div>
        <div class="collapse navbar-collapse" id="myNavbar">
            <ul class="nav navbar-nav navbar-right">
                <li><a href="http://localhost:63342/UniversityRankingSystem/page.html"> <span class="glyphicon glyphicon-chevron-left"></span> BACK TO HOME</a></li>
                <li><a href="#see">SEARCH RESULTS</a></li>
                <!--                <li><a href="#info"><span class="fa fa-info-circle"></span></a></li>-->
            </ul>
        </div>
    </div>
</nav>

<h3 style="margin-left: 490px;margin-top: 100px">University Ranking System Database</h3>
<p style="color: gold;text-shadow: 0 0 3px lime;margin-left: 494px">The world's top Universities compared on various metrics.</p>
<div class="row">
    <div class="col-sm-8" style="margin-left: 220px" >
        <div class="panel panel-primary">
            <div id="see"class="panel-body">








<?php
$servername = "127.0.0.1";
$username = "root";
$password = '';
$dbname = "myDB";

// Create connection
$conn = new mysqli($servername, $username, $password, $dbname);
// Check connection
if ($conn->connect_error) {
    die("Connection failed: " . $conn->connect_error);
}

$sql = "SELECT Institution, Country, Rank, Faculty, Infrastructure, Cocurricular, Time, Date FROM University where {$_POST['field_to_search']}='{$_POST['value_to_search']}'";
$result = $conn->query($sql);

if ($result->num_rows > 0) {
    // output data of each row


	echo "<style> table, th, td {
    border: 1px solid black;
    }</style>";
	echo "<table id='see' style='margin-top: 0px' class='table table-striped table-bordered table-hover'><thead>";
	echo "<tr><th><ins>Institution</ins></th>"."<th><ins>Country</ins></th>"."<th><ins>Academics</ins></th>"."<th><ins>Faculty</ins></th>"."<th><ins>Infrastructure</ins></th><th><ins>Cocurricular</ins></th>".""."<th><ins>Time</ins></th><th><ins>Date</th></tr></ins></thead>"."<br>";
    while($row = $result->fetch_assoc()) {
        echo "<tr  style='color:red;'><td>".$row["Institution"]. "</td><td>" . $row["Country"]. "</td><td>" . $row["Rank"]. "</td><td>" . $row["Faculty"]. "</td><td>" . $row["Infrastructure"]. "</td><td>" . $row["Cocurricular"]. "</td><td>". $row["Time"]. "</td><td>".$row["Date"]."</td></tr>". "<br>";
    }
	echo"</table>";
} else {
    echo "0 results";
}

echo "<a class='btn btn-default'style='margin-left: 40%' href='http://localhost:63342/UniversityRankingSystem/select2.php'>Click to See All Rankings!</a><br>";

$conn->close();
?>
            </div>
        </div></div></div><br><br>
<!-- Footer -->
<footer class="text-center">
    <div class="container bootstrap snippet">

        <div class="alert alert-info " role="alert" style="height: 60px">
            <div style="float:left;margin-right:10px;">
                <i id="info" class="fa fa-info-circle"></i>
            </div>
            <div style="float:left;width:1050px">
                <marquee>In case of dicrepancies, contact Umang Gupta (rollno 106114098)</marquee>
            </div>
        </div>

    </div>
    <a class="up-arrow" href="#myPage" data-toggle="tooltip" title="TO TOP">
        <span class="glyphicon glyphicon-chevron-up"></span>
    </a><br><br>
    <p>University Ranking System, Made By <a data-toggle="tooltip" title="Umang Gupta 106114098">Umang Gupta (106114098)</a></p>
</footer>

</body>
</html>

